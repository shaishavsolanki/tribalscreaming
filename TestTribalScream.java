import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class TestTribalScream {

	
	// Expected output Peter is Amazing
	@Test
	void testPeterisAmazing() {
		
		TribalScream tribal = new TribalScream();
		String actualoutput = tribal.scream("Peter");
		
		assertEquals("Peter is Amazing",actualoutput);
		
		
	}
	
	//Expected output Pritesh
	@Test
	void testPeterisNotAmazing() {
		
		TribalScream tribal = new TribalScream();
		String actualoutput = tribal.scream("Pritesh");
		
		assertEquals("Pritesh",actualoutput);
		
		
	}
	
	//Test 2 You are amazing
	
	@Test
	void testYouareAmazing() {
		
		TribalScream tribal = new TribalScream();
		String actualoutput = tribal.scream(" ");
		
		assertEquals("You are Amazing",actualoutput);
	}
	
	//Test 3 Peter is Shouting 
	//Expected output PETER IS AMAZING
	
	@Test
	void testPeterShouted() {
		
		TribalScream tribal = new TribalScream();
		String actualoutput = tribal.shouting("PETER");
		
		assertEquals("PETER IS AMAZING",actualoutput);
	}
	
	
	//Test 4 
	
	@Test
	void testTwoPeopleAreAmazing() {
		
		String[] two = new String[]{"Peter", "Jigisha"};
		TribalScream tribal = new TribalScream();
		String actualoutput = tribal.twoStrings(two);
		
		assertEquals("Peter and Jigisha are Amazing",actualoutput);
	}
	
	//Test 4 Fail
	
	@Test 
	void testTwoPeopleAreNotAmazing() {
		String[] two = new String[]{"Peter", "Jigisha"};
		TribalScream tribal = new TribalScream();
		String actualoutput = tribal.twoStrings(two);
		
		assertEquals("Peter and Jigisha are Amazing",actualoutput);
	}
	

	//Test 5 
	@Test
	void testEvery1isAmazing(){
		
		String[] more_than_two = new String[]{"Peter", "Jigisha", "Marcos"};
		 	
		TribalScream tribal = new TribalScream();
		String actualoutput = tribal.morethanTwo(more_than_two);
		
		assertEquals("Peter, Jigisha, and Marcos are Amazing",actualoutput);
	} 
	
	//Test 6
	@Test
	void testAmazing() {
		String[] more_than_two_uppercase = new String[]{"Peter", "JIGISHA", "Marcos", "Pritesh"};
		
		TribalScream tribal = new TribalScream();
		String actualoutput = tribal.morethanTwoUppercase(more_than_two_uppercase);
		
		assertEquals("Peter, Marcos, and Pritesh are Amazing JIGISHA ALSO!",actualoutput);
		
	}
}
