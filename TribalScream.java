
public class TribalScream {

	
	//Test 1 Peter is Amazing
	//Test 2 You are Amazing
	public String scream(String name){
		if(name == "Peter"){
			return name + " is Amazing";
		}
		
		else if (name == " "){
			return "You are Amazing";
		}	
		else {
			return name;
		}
	}
	
	public String shouting(String name) {
		return name + " IS AMAZING";
	}
	
	
	//Test 4
	
	public String twoStrings(String[] persons){
		if(persons.length>2) {
			return "";
		}
		return persons[0] + " and " + persons[1] + " are Amazing";
	}
	
	
	//Test 5
	
	public String morethanTwo(String[] persons){
		String returnMessage = "";
		for(int i=0; i<persons.length-1; i++){
			returnMessage = returnMessage + persons[i] + ", ";
			
		}
		returnMessage = returnMessage + "and " + persons[persons.length-1] + " are Amazing";
		
		return returnMessage;
	}
	
	
	//Test6
	
	public String morethanTwoUppercase(String[] persons){
		String returnMessage = "";
		String upper = persons[persons.length - 1];
		
		for(int i=0; i<persons.length-1; i++){
			if(checkUppercase(persons[i])){
				upper = persons[i];
				
				for(int j=i; j<persons.length-1; j++){
					persons[j] = persons[j+1];
				}
				persons[persons.length-1] = upper;
			}
		}
		
		// Making String
		for(int i=0; i<persons.length-2; i++){
			returnMessage += persons[i] + ", ";
		}
		returnMessage += "and " + persons[persons.length - 2] + " are Amazing " + upper + " ALSO!";
	
		return returnMessage;
	}
	
	
	
	//Checking if the name is in UPPERCASE
	public boolean checkUppercase(String str){
		for(int i=0; i<str.length(); i++){
			char c = str.charAt(i);
			//Giving the ASCII value of letter
			if(c >= 97 && c <= 122) {
				return false;
			}
		}
		return true;
	}
	
	
	
}
